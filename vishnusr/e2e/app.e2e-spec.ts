import { VishnusrPage } from './app.po';

describe('vishnusr App', () => {
  let page: VishnusrPage;

  beforeEach(() => {
    page = new VishnusrPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

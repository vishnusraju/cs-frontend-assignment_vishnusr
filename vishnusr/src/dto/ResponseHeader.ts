import {JWT} from "./JWT";

export class ResponseHeader{
    status:string;
    token:string;
    message:string;
    jwt:JWT;
}

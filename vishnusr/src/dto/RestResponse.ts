import {ResponseHeader} from "./ResponseHeader";
export class RestResponse<T>{
    responseHeader:ResponseHeader;
    responseBody:T;
    cart:T;
    currency_code :String;
    status:String;
}
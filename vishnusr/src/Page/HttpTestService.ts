import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions,Response  } from '@angular/http';



@Injectable()
export class HttpTestServices    {
    
constructor(private http:Http){
}
   
   postJson(){
    
        var json =JSON.stringify ({"country_code":"de"});
        var params=json;
        var headers=new Headers();
        headers.append('Content-type','application/x-www-form-urlencoded');
        
            return this.http.post('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/',params,{headers:headers})
        .map(Response => Response.json());
        
}

putJson(){

     var json =JSON.stringify ({ "articles": [ { "article_id": 88, "article_quantity": 1 } ], "language_code": "de", "currency_code": "EUR", "country_code":"de" });
        var params1=json;
        var headers=new Headers();
        headers.append('Content-type','application/x-www-form-urlencoded');
        
            return this.http.put('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/eJwNyTuWAjAIAMC7pLbgmxBvAwR8NhbqVvu8u7Yz_5la5h_p1u5N1vz38_5la5hfescR1RZ5oylpDZnOFIVT8La0iLxRriXoe2KnqRHrZfUwivks7pgLtyLmvcBtBrS6w_p1u5J_p1u5Okmjs7eie0AmNyB2gfoAheshN9XEb6830_5la5h44qySQGNWFjm0kmfLyePMP8/',params1,{headers:headers})
        .map(Response => Response.json());


} 

  
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {HttpTestServices} from '../Page/HttpTestService';
import {FirstPage} from '../Page/FirstPage';
import {SecondPage} from '../Page/SecondPage';
import { AppComponent } from './app.component';


const appRoutes:Routes=[{path:'Second',component:SecondPage},{path:'',component:FirstPage}];
@NgModule({
  declarations: [
    AppComponent,
   SecondPage,
    FirstPage,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [HttpTestServices],
  bootstrap: [FirstPage]
})
export class AppModule { }
